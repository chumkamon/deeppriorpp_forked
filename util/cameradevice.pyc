ó
3¸[c           @   só   d  Z  d d l Z d d l Z d d l Z d d l Z d d l Z d d l Z d Z	 d Z
 d g Z d Z d Z d Z d Z d	 Z d
 e f d     YZ d e f d     YZ d e f d     YZ d e f d     YZ d e f d     YZ d S(   s  Basis for depth camera devices.

CameraDevice provides interface for managing depth cameras.
It can be used to retrieve basic information and read
depth and color frames.

Copyright 2015 Markus Oberweger, ICG,
Graz University of Technology <oberweger@icg.tugraz.at>

This file is part of DeepPrior.

DeepPrior is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DeepPrior is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with DeepPrior.  If not, see <http://www.gnu.org/licenses/>.
iÿÿÿÿNs*   Markus Oberweger <oberweger@icg.tugraz.at>s;   Copyright 2015, ICG, Graz University of Technology, Austrias   Markus Oberwegert   GPLs   1.0s   oberweger@icg.tugraz.att   Developmentt   CameraDevicec           B   s   e  Z d  Z e d  Z d   Z d   Z d   Z d   Z d   Z	 d   Z
 d   Z d	   Z d
   Z d   Z d   Z d   Z d   Z RS(   s8   
    Abstract class that handles all camera devices
    c         C   s   | |  _  d S(   sb   
        Initialize device
        :param mirror: mirror all images
        :return: None
        N(   t   mirror(   t   selfR   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   __init__1   s    c         C   s   t  d   d S(   s4   
        Start device
        :return: None
        t   !N(   t   NotImplementedError(   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   start:   s    c         C   s   t  d   d S(   s3   
        Stop device
        :return: None
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   stopA   s    c         C   sT   t  j j | j d  d t j |  d t j |  d d } | j | d  d S(   s®   
        Save data to file, we need special treatment because we have 16bit depth
        :param data: data
        :param file_name: file name
        :return: None
        t   uint16t   hight   lowt   modet   Is   .pngN(   t   scipyt   misct   toimaget   astypet   numpyt   maxt   mint   save(   R   t   datat	   file_namet   im(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt	   saveDepthH   s    ?c         C   s6   t  | j  d k s t  t j j | d |  d S(   s   
        Save data to file 3x8bit color
        :param data: data
        :param file_name: file name
        :return: None
        i   s   .pngN(   t   lent   shapet   AssertionErrorR   R   t   imsave(   R   R   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   saveRGBT   s    c         C   s   t  d   d S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getDeptha   s    c         C   s   t  d   d S(   sV   
        Return a bit color image
        :return: color image as numpy array
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getRGBh   s    c         C   s   t  d   d S(   sZ   
        Return a grayscale image
        :return: grayscale image as numpy array
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getGrayScaleo   s    c         C   sI   |  j    \ } } |  j   \ } } | o- | | j d  | j d  f S(   sd   
        Return a color + depth image
        :return: RGB-D image as 4-channel numpy array
        t   float32(   R!   R    R   (   R   t   ret_rgbt   ct   ret_dt   d(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getRGBDv   s    c         C   s   t  d   d S(   sT   
        Get frame number of last color frame
        :return: frame number
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getLastColorNum   s    c         C   s   t  d   d S(   sT   
        Get frame number of last depth frame
        :return: frame number
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getLastDepthNum   s    c         C   s   t  d   d S(   sc   
        Get intrinsic matrix of depth camera
        :return: 3x3 intrinsic camera matrix
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getDepthIntrinsics   s    c         C   s   t  d   d S(   sc   
        Get intrinsic matrix of color camera
        :return: 3x3 intrinsic camera matrix
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getColorIntrinsics   s    c         C   s   t  d   d S(   sn   
        Get extrinsic matrix from color to depth camera
        :return: 4x3 extrinsic camera matrix
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   getExtrinsics   s    (   t   __name__t
   __module__t   __doc__t   FalseR   R   R	   R   R   R    R!   R"   R(   R)   R*   R+   R,   R-   (    (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR   ,   s   													t   CreativeCameraDevicec           B   st   e  Z d  Z e d  Z d   Z d   Z d   Z d   Z d   Z	 d   Z
 d   Z d	   Z d
   Z d   Z RS(   sB    DepthSense camera class, for Creative Gesture Camera, DS325, etc.c         C   s   t  t |   j |  d S(   sG   
        Initialize device
        :param mirror: mirror image
        N(   t   superR2   R   (   R   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR   ¨   s    c         C   s   t  j   d S(   s4   
        Start device
        :return: None
        N(   t   dscR   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR   °   s    c         C   s   t  j   d S(   s3   
        Stop device
        :return: None
        N(   R4   R	   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR	   ·   s    c         C   sw   |  j  r1 t j   d d  d d d  f } n t j   } t j | d  } t j |  d k t j | t j  f S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        Niÿÿÿÿi   i    (	   R   R4   t   getDepthMapt   cv2t
   medianBlurR   t   count_nonzerot   asarrayR#   (   R   t   depth(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR    ¾   s
    	(c         C   s_   |  j  r: t j   d d  d d d  d d  f } n t j   } t j |  d k | f S(   sV   
        Return a bit color image
        :return: color image as numpy array
        Niÿÿÿÿi    (   R   R4   t   getColourMapR   R8   (   R   t   image(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR!   Ë   s    	1c         C   sz   |  j  r: t j   d d  d d d  d d  f } n t j   } t j | t j  } t j |  d k | j   f S(   sZ   
        Return a grayscale image
        :return: grayscale image as numpy array
        Niÿÿÿÿi    (	   R   R4   t   getColorMapR6   t   cvtColort   COLOR_BGR2GRAYR   R8   t	   transpose(   R   R<   t   grey(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR"   ×   s
    	1c         C   s
   t  j   S(   sT   
        Get frame number of last color frame
        :return: frame number
        (   R4   R)   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR)   ä   s    c         C   s
   t  j   S(   sT   
        Get frame number of last depth frame
        :return: frame number
        (   R4   R*   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR*   ë   s    c         C   s
   t  j   S(   sc   
        Get intrinsic matrix of depth camera
        :return: 3x3 intrinsic camera matrix
        (   R4   R+   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR+   ò   s    c         C   s
   t  j   S(   sc   
        Get intrinsic matrix of color camera
        :return: 3x3 intrinsic camera matrix
        (   R4   R,   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR,   ú   s    c         C   s
   t  j   S(   sn   
        Get extrinsic matrix from color to depth camera
        :return: 4x3 extrinsic camera matrix
        (   R4   R-   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR-     s    (   R.   R/   R0   R1   R   R   R	   R    R!   R"   R)   R*   R+   R,   R-   (    (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR2   ¥   s   									t   DepthSenseCameraDevicec           B   s>   e  Z d  Z e d  Z d   Z d   Z d   Z d   Z RS(   sA   
    Class for OpenNI based devices, e.g. Kinect, Asus Xtion
    c         C   s   t  t |   j |  d S(   sG   
        Initialize device
        :param mirror: mirror image
        N(   R3   RB   R   (   R   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR     s    c         C   s¯   t  j   |  _ |  j j   t  j   |  _ |  j j |  j  |  j j t  j  d |  j _	 t  j
   |  _ |  j j |  j  |  j j t  j  d |  j _	 |  j j   d S(   s3   
        Stop device
        :return: None
        i   N(   t   opennit   Contextt   ctxt   initt   DepthGeneratorR:   t   createt   set_resolution_presett   RES_VGAt   fpst   ImageGeneratort   colort   start_generating_all(   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR     s    c         C   s   |  j  j   |  j  j   d S(   s3   
        Stop device
        :return: None
        N(   RE   t   stop_generating_allt   shutdown(   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR	   1  s    c         C   s   y |  j  j |  j  Wn t j k
 r8 } d G| GHnG Xt j |  j j   d d j |  j j	 j
 |  j j	 j  } t | f Sd S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        s   Failed updating data:t   dtypeR#   N(   RE   t   wait_one_update_allR:   RC   t   OpenNIErrorR   R9   t   get_tuple_depth_mapt   reshapet   mapt   heightt   widtht   True(   R   t   errt   dpt(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR    :  s    <c         C   s   y |  j  j |  j  Wn t j k
 r8 } d G| GHnG Xt j |  j j   d d j |  j j	 j
 |  j j	 j  } t | f Sd S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        s   Failed updating data:RQ   R#   N(   RE   RR   RM   RC   RS   R   R9   RT   RU   RV   RW   RX   RY   (   R   RZ   R[   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR!   K  s    <(	   R.   R/   R0   R1   R   R   R	   R    R!   (    (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyRB   	  s   				t   RealsenseCameraDevicec           B   s>   e  Z d  Z e d  Z d   Z d   Z d   Z d   Z RS(   s&   
    Class for Realsense devices,
    c         C   s   t  t |   j |  d S(   sG   
        Initialize device
        :param mirror: mirror image
        N(   R3   R\   R   (   R   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR   `  s    c         C   sy   t  j   |  _ t  j   } | j t  j j d d t  j j d  | j t  j j d d t  j j	 d  |  j j
 |  d S(   s/   
        Start device
        :return:
        i  ià  i   N(   t   rst   pipelinet   configt   enable_streamt   streamR:   t   formatt   z16RM   t   bgr8R   (   R   R_   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR   g  s
    %%c         C   s   |  j  j   d S(   s.   
        Stop device
        :return:
        N(   R^   R	   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR	   s  s    c         C   sW   y |  j  j   } | j   } Wn d GHn& Xt j | j   d d } t | f Sd S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        s   Failed updating depth data:RQ   R#   N(   R^   t   wait_for_framest   get_depth_frameR   t
   asanyarrayt   get_dataRY   (   R   t   framest   depth_frameR[   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR    z  s    	c         C   s   t  d   d S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR!     s    (	   R.   R/   R0   R1   R   R   R	   R    R!   (    (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR\   \  s   			t
   FileDevicec           B   s}   e  Z d  Z e d  Z d   Z d   Z d   Z d   Z d   Z	 d   Z
 d   Z d	   Z d
   Z d   Z d   Z RS(   s-   
    Fake class to load images from file
    c         C   s   t  t |   j |  t | t  s4 t d   n  | |  _ | |  _ | j   |  _	 t
 j d  |  _ t
 j d  |  _ | |  _ d |  _ d |  _ d S(   sb   
        Initialize device
        :param mirror: mirror all images
        :return: None
        s    Files must be list of filenames.i   i   i    N(   i   i   (   i   i   (   R3   Rk   R   t
   isinstancet   listt
   ValueErrort	   filenamest   importert   getCameraIntrinsicst   depth_intrinsicsR   t   zerost   color_intrinsicst
   extrinsicsR   t   last_color_numt   last_depth_num(   R   Ro   Rp   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR     s    				c         C   s   d S(   s4   
        Start device
        :return: None
        N(    (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR   ­  s    c         C   s   d S(   s3   
        Stop device
        :return: None
        N(    (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR	   ´  s    c         C   sB   t  j d  |  j j |  j |  j  } |  j d 7_ t | f S(   sa   
        Return a median smoothed depth image
        :return: depth data as numpy array
        g{®Gáz?i   (   t   timet   sleepRp   t   loadDepthMapRo   Rw   RY   (   R   t   frame(    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR    »  s    c         C   s   t  d   d S(   sV   
        Return a bit color image
        :return: color image as numpy array
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR!   Å  s    c         C   s   t  d   d S(   sZ   
        Return a grayscale image
        :return: grayscale image as numpy array
        R   N(   R   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR"   Ì  s    c         C   sI   |  j    \ } } |  j   \ } } | o- | | j d  | j d  f S(   sd   
        Return a color + depth image
        :return: RGB-D image as 4-channel numpy array
        R#   (   R!   R    R   (   R   R$   R%   R&   R'   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR(   Ó  s    c         C   s   |  j  S(   sT   
        Get frame number of last color frame
        :return: frame number
        (   Rv   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR)   Þ  s    c         C   s   |  j  S(   sT   
        Get frame number of last depth frame
        :return: frame number
        (   Rw   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR*   å  s    c         C   s   |  j  S(   sc   
        Get intrinsic matrix of depth camera
        :return: 3x3 intrinsic camera matrix
        (   Rr   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR+   ì  s    c         C   s   |  j  S(   sc   
        Get intrinsic matrix of color camera
        :return: 3x3 intrinsic camera matrix
        (   Rt   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR,   ó  s    c         C   s   |  j  S(   sn   
        Get extrinsic matrix from color to depth camera
        :return: 4x3 extrinsic camera matrix
        (   Ru   (   R   (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyR-   ú  s    (   R.   R/   R0   R1   R   R   R	   R    R!   R"   R(   R)   R*   R+   R,   R-   (    (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyRk     s   			
							(   R0   Rx   R   R6   t
   scipy.miscR   t   pyrealsense2R]   RC   t
   __author__t   __copyright__t   __credits__t   __license__t   __version__t   __maintainer__t	   __email__t
   __status__t   objectR   R2   RB   R\   Rk   (    (    (    s8   /home/john/worksp/deep-prior-pp/src/util/cameradevice.pyt   <module>   s&   	ydS6